package com.example.gestioninventariomargu;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.gestioninventariomargu.SQLite.SQLite;
import com.example.gestioninventariomargu.SQLite.SQLite_Calls;
import com.example.gestioninventariomargu.utils.AdaptadorAlbaranes;
import com.example.gestioninventariomargu.utils.AdaptadorObjetos;
import com.example.gestioninventariomargu.utils.AlbaranDetalles;
import com.example.gestioninventariomargu.utils.Conexion;
import com.example.gestioninventariomargu.utils.ObjFromSSMS;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.stream.Collectors;

import kotlin.reflect.KType;

public class SalidaActivity extends AppCompatActivity {

    Toolbar toolbar;
    ImageButton logo;
    TextView syso;
    ObjFromSSMS albaran;
    EditText scanedCode;
    ArrayList<AlbaranDetalles> AlbaranesDetallesList;
    RecyclerView historyRecView;
    AdaptadorAlbaranes adaptadorAlbaranesDetalles;
    Button guardarCambios;
    boolean forzarSalida = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_salida);

        //con esto añado el toolbar para poder poner el logo y el boton de "atras"
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);

        //pequeño listener para volver a home si pulsamos el logo
        logo = findViewById(R.id.logo);
        logo.setOnClickListener(view -> {
            goBack();
        });

        //Recuperamos el cliente en que se ha pulsado
        Intent intent = getIntent();
        albaran = intent.getParcelableExtra("albaran");

        //variables
        syso = findViewById(R.id.syso);
        scanedCode = findViewById(R.id.scanedCode);
        guardarCambios = findViewById(R.id.guardarCambios);

        syso.setText(albaran.getNombreObj());
        scanedCode.setShowSoftInputOnFocus(false);
        scanedCode.setInputType(0);

        recuperarAlbaranes(); //Recupero listado de albaranes de BD
        adaptadorAlbaranesDetalles = new AdaptadorAlbaranes(AlbaranesDetallesList); //Y genero un adaptador con esa lista

        //Controles del RecicleView para mostrar el listado
        historyRecView = findViewById(R.id.historyRecView);
        historyRecView.setHasFixedSize(true);
        historyRecView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        historyRecView.setAdapter(adaptadorAlbaranesDetalles);
        //DividerItemDecoration divider = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        //historyRecView.addItemDecoration(divider);

        //Cuando hago click en un albaran
        adaptadorAlbaranesDetalles.setOnClickListener(view -> {
            int pos = historyRecView.getChildAdapterPosition(view);
            //scanOne(pos); //Debug
            Log.i("syso", AlbaranesDetallesList.get(pos).toString());
        });

        //guardo los cambios en base de datos
        guardarCambios.setOnClickListener(view -> {
            int toastText;
            switch (saveList()){
                case 2:
                    toastText = R.string.save_error;
                    break;
                case 1:
                    toastText = R.string.save_false;
                    break;
                default:
                    toastText = R.string.save_ok;
                    goBack();
                    break;
            }

            Toast.makeText(this,toastText,Toast.LENGTH_LONG).show();
        });
    }

    public void recuperarAlbaranes() {
        AlbaranesDetallesList = new ArrayList<>();
        //AlbaranesDetallesList.add(new AlbaranDetalles()); //Añade cabecera a la tabla
        Connection con = Conexion.getConnection(this);
        try {
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery("SELECT ad.id,alba_pakinglist," +
                    "CASE WHEN m.caja is NULL THEN 1 ELSE m.caja END AS caja,"+
                    "CASE WHEN total_palet = -1 THEN CONCAT(cantidad , ' ' , unidadventa) ELSE CONCAT(total_palet , ' palets') END AS texto, " +
                    "CASE WHEN picking = 'S' THEN cantidad ELSE total_palet END AS total, " +
                    "CASE WHEN picking = 'S' THEN CASE WHEN por_cargar = -1 THEN 0 ELSE cantidad - por_cargar END ELSE total_palet - por_cargar END AS cargados, " +
                    "CASE WHEN picking is NULL OR picking = 'N' THEN 0 ELSE CASE WHEN substring(unidadventa, 1, 3) <> 'Caj' THEN 2 ELSE 1 END END AS picking, " +
                    "articulo " +
                    "FROM TBL_ALBARAN_DETALLE as ad " +
                    "inner join TBL_ALBARAN as a  on ad.id_albaran = a.id "+
                    "LEFT JOIN TBL_CODIGOS_FLOREX as cf on ad.alba_pakinglist = cf.cod_barras and cf.cod_interno = a.tipo " +
                    "LEFT JOIN (SELECT CODIGO, CAJA FROM TBL_MAESTRO ) m ON m.codigo = cf.articulo "+
                    "where id_albaran = "+albaran.getIdObj()+";");
            while (rs.next()) {
                if(rs.getInt("total") - rs.getInt("cargados") != 0){
                    AlbaranesDetallesList.add(new AlbaranDetalles(rs.getInt("id"), rs.getLong("alba_pakinglist"), rs.getString("texto"), rs.getInt("total"), rs.getInt("cargados"), rs.getInt("picking"), rs.getInt("caja"), rs.getString("articulo")));
                }
            }
        } catch (SQLException e) {
            Log.e("syso", "select from TBL_ALBARAN_DETALLE: " + e.getMessage());
            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
        } catch (Exception e){
            Log.e("syso", "recuperarAlbaranes Exception: " + e.getMessage());
            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    //Recogemos el evento que se ejecuta cada vez que se va a escribir algo
    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        //Log.i("syso", "Codigo escaneado: "+event.getKeyCode()); //debuging
        if(event.getKeyCode()==62){ //Buscamos el keycode que hemos indicado como fin del escaner
            String code = scanedCode.getText().toString();
            code = code.replaceAll("[\\D?!]",""); //Limpiamos el string
            MediaPlayer mediaPlayer = MediaPlayer.create(this, R.raw.beepbeep); //Preparamos el sonido de error

            //Si es EAN 128 lo pasamos a EAN 13
            if(code.length() == 28){
                code = code.substring(3, 16);
            } else if (code.length()==27) {
                code = code.substring(2, 15);
            } else if (code.length()==14) {
                code = code.substring(1);
            }

            if(code.length() >= 13 && code.length() <= 14){ //Comprobamos que se haya escaneado EAN 13
                for (AlbaranDetalles a : AlbaranesDetallesList) { //Luego compruebo que ese algo este en la lista
                    if(a.getAlba_pakinglist() == Long.parseLong(code)){
                        if(scanOne(AlbaranesDetallesList.indexOf(a))) mediaPlayer = MediaPlayer.create(this, R.raw.beep); //Si esta cambio el sonido
                        break;
                    }
                }
            }

            scanedCode.setText(""); //limpiamos el Edit Text al final
            if(code.length() > 0) mediaPlayer.start(); //Reproduzco el sonido si se escaneo algo
        }
        return super.dispatchKeyEvent(event); //devolvemos el evento para que siga el proceso normal
    }

    private boolean scanOne(int pos){
        if(AlbaranesDetallesList.get(pos).scanOne()){ //reduzco uno a la cantidad de restantes
            adaptadorAlbaranesDetalles.notifyItemChanged(pos); //si se pudo cambiar indico que algo cambio y devuelvo true
            testFinish(pos);
            return true;
        }
        return false;
    }

    private void testFinish(int pos){
        if(AlbaranesDetallesList.get(pos).getFinalizado()) {

        }
    }

    private int saveList() {
        Connection con = Conexion.getConnection(this); //Recupero la conexion
        if (con != null) {
            try {
                //Defino algunas variables para ahorrar tiempo de procesamiento
                String pda_name = SQLite_Calls.getPdaName(SQLite.getDB(this));
                String date = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(new Date());
                String time = new SimpleDateFormat("HH:mm", Locale.getDefault()).format(new Date());
                String n_albaran = albaran.getNombreObj().split("\n")[2];

                int result = 1; //guardo que no había ningun registro

                //Indico los codigos escaneados
                PreparedStatement pst = con.prepareStatement("update TBL_ALBARAN_DETALLE set por_cargar = ? , fecha_salida = ? , hora_salida = ? , terminal = ? where id = ?;"); //Preparo el update sin datos
                for (AlbaranDetalles a : AlbaranesDetallesList) {
                    if(a.getEscaneados_ahora() > 0){ //si ha habido algun cambio
                        result = 0; //guardo el "okay"

                        int i = 0; //para rellenar el update sin preocuparme de numeros, solo de posiciones
                        pst.setInt(++i, a.getPor_Cargar());
                        pst.setString(++i, date);
                        pst.setString(++i, time);
                        pst.setString(++i, pda_name);
                        pst.setInt(++i, a.getId());
                        pst.execute(); //ejecuto el update con los datos indicados

                        if(a.getPicking() != 0){ //relleno la tabla almacen de forma diferente para picking y para palet
                            PreparedStatement pst2 = con.prepareStatement("insert into TBL_ALMACEN (codigo, salida, pakinglist, hora_salida, fecha_salida, terminal_salida, n_albaran, a_picking, unidades) values (?,2,?,?,?,?,?,?,?);");
                            int j = 0;
                            pst2.setString(++j, ""+a.getAlba_pakinglist());
                            pst2.setString(++j, ""+a.getAlba_pakinglist());
                            pst2.setString(++j, time);
                            pst2.setString(++j, date);
                            pst2.setString(++j, pda_name);
                            pst2.setString(++j, n_albaran);
                            pst2.setString(++j, "S");
                            pst2.setString(++j, "-"+a.getEscaneados_ahora());
                            pst2.execute();
                        }else{
                            Statement st = con.createStatement();
                            ResultSet rs = st.executeQuery("SELECT TOP "+a.getEscaneados_ahora()+" id FROM TBL_ALMACEN where pakinglist = '"+a.getAlba_pakinglist()+"' and salida = 0 and (a_picking is null or a_picking = 'N');"); //recuperon tantos pales como necesite
                            PreparedStatement pst2 = con.prepareStatement("update TBL_ALMACEN set salida = 2 , hora_salida = ? , fecha_salida = ? , terminal_salida = ? , n_albaran = ? where id = ?;");
                            while (rs.next()) { //Y luego actualizo cada uno de los palets recuperados con nuevos datos
                                int j = 0;
                                pst2.setString(++j, time);
                                pst2.setString(++j, date);
                                pst2.setString(++j, pda_name);
                                pst2.setString(++j, n_albaran);
                                pst2.setInt(++j, rs.getInt("id"));
                                pst2.execute();
                            }
                        }
                        a.setEscaneados_ahora(0);
                    }
                }
                cerrarAlbaran(date, time);
                return result; //si no hubo fallos devuelvo el result
            } catch (SQLException e) {
                Log.e("syso", "update TBL_ALBARAN_DETALLE/TBL_ALMACEN: " + e.getMessage());
                Log.e("syso", "update TBL_ALBARAN_DETALLE/TBL_ALMACEN: " + e);
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
            } catch (Exception e){
                Log.e("syso", "saveList Exception: " + e.getMessage());
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
            }
            //si llegue aqui, y la conexion no era null, significa que hubo un fallo de conexion y hay que volver a crearla
            Conexion.createFromDB(this);
        }
        return 2; //Si no había conexion o algo fallo al intentar guardarlos devuelvo un 2
    }

    public void cerrarAlbaran(String date, String time){
        boolean watcher = true;
        for(AlbaranDetalles a :AlbaranesDetallesList){
            if(!a.getFinalizado()){
                watcher = false;
            }
        }
        if(watcher){
            try {
                Connection con = Conexion.getConnection(this); //Recupero la conexion
                PreparedStatement pst = con.prepareStatement("update TBL_ALBARAN set salida = ?, fecha_terminado = ?, hora_terminado = ? where id = ?;");
                pst.setInt(1, 1);
                pst.setString(2, date);
                pst.setString(3, time);
                pst.setInt(4, albaran.getIdObj());
                pst.executeUpdate(); //ejecuto el query
            } catch (SQLException e) {
                Log.e("syso", "update TBL_ALBARAN: " + e.getMessage());
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
            } catch (Exception e){
                Log.e("syso", "cerrarAlbaran Exception: " + e.getMessage());
                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onBackPressed() { //Capturo el evento de "back press" para guardar los codigos escaneados antes
        if(forzarSalida){ //si pulso "salir" en el dialog
            super.onBackPressed();
            return;
        }

        boolean cambios = false; //Compruebo si hay algun codigo que cambiar
        for (AlbaranDetalles a : AlbaranesDetallesList) {
            if(a.getEscaneados_ahora() > 0){
                cambios = true;
            }
        }

        if (cambios){
            alert().show(); //Si lo hay muestro el alert
        } else {
            super.onBackPressed();
        }
    }

    private Dialog alert(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.dialog_message); //mensaje
        builder.setPositiveButton(R.string.dialog_save, (dialog, id) -> { //boton ok
            if(saveList() == 0 || saveList() == 1) goBack();
        });
        builder.setNegativeButton(R.string.dialog_cancel, (dialog, id) -> { //boton cancel
            forzarSalida = true; //indico que voy a forzar la salida
            goBack(); //Y ejecuto el metodo para salir
        });
        return builder.create(); //Lo creo y lo devuelvo para mostrarlo
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) { //Indico que voy a usar un menu en el Toolbar
        getMenuInflater().inflate(R.menu.back, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) { //Le doy funcionalidad a los items del menu
        int id = item.getItemId();

        if(id == R.id.back){
            goBack();
        }

        return true;
    }

    //Controles para cargar otras activities
    private void goBack(){
        this.onBackPressed();
    }

    private void goToHome(){
        Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
        startActivity(intent);
    }
}