package com.example.gestioninventariomargu.utils;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.gestioninventariomargu.R;

import java.util.ArrayList;

public class AdaptadorObjetos extends RecyclerView.Adapter<AdaptadorObjetos.ObjetosViewHolder> implements View.OnClickListener{
    private ArrayList<ObjFromSSMS> datos;
    private View.OnClickListener listener;

    public AdaptadorObjetos(ArrayList<ObjFromSSMS> datos){
        this.datos = datos;
    }

    public void setOnClickListener(View.OnClickListener listener) {
        this.listener = listener;
    }

    @Override
    public ObjetosViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recicle_view_layout, parent, false);
        itemView.setOnClickListener(this);
        ObjetosViewHolder vh = new ObjetosViewHolder(itemView);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ObjetosViewHolder holder, int position) {
        ObjFromSSMS obj = datos.get(position);
        holder.bindObj(obj);
    }

    @Override
    public int getItemCount() {
        return datos.size();
    }

    @Override
    public void onClick(View view) {
        if(listener != null){
            listener.onClick(view);
        }
    }

    public static class ObjetosViewHolder extends RecyclerView.ViewHolder{
        private TextView nombreObj;
        private TextView idObj;

        public ObjetosViewHolder(View itemView){
            super(itemView);
            nombreObj = (TextView) itemView.findViewById(R.id.nombreObj);
            idObj = (TextView) itemView.findViewById(R.id.idObj);
        }

        public void bindObj(ObjFromSSMS c){
            nombreObj.setText(c.getNombreObj());
            idObj.setText(""+c.getIdObj());
        }
    }
}
