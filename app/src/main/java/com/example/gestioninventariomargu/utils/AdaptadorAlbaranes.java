package com.example.gestioninventariomargu.utils;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.gestioninventariomargu.R;

import java.util.ArrayList;

public class AdaptadorAlbaranes extends RecyclerView.Adapter<AdaptadorAlbaranes.AlbaranesViewHolder> implements View.OnClickListener{
    private ArrayList<AlbaranDetalles> datos;
    private View.OnClickListener listener;

    public AdaptadorAlbaranes(ArrayList<AlbaranDetalles> datos){
        this.datos = datos;
    }

    public void setOnClickListener(View.OnClickListener listener) {
        this.listener = listener;
    }

    @Override
    public AlbaranesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recicle_view_albaranes, parent, false);
        itemView.setOnClickListener(this);
        AlbaranesViewHolder vh = new AlbaranesViewHolder(itemView);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull AlbaranesViewHolder holder, int position) {
        AlbaranDetalles obj = datos.get(position);
        holder.bindAlbaran(obj);
    }

    @Override
    public int getItemCount() {
        return datos.size();
    }

    @Override
    public void onClick(View view) {
        if(listener != null){
            listener.onClick(view);
        }
    }

    public static class AlbaranesViewHolder extends RecyclerView.ViewHolder{
        private TextView articulo;
        private TextView alba_pakinglist;
        private TextView texto;
        private TextView cargados;

        public AlbaranesViewHolder(View itemView){
            super(itemView);
            articulo = (TextView) itemView.findViewById(R.id.articulo);
            alba_pakinglist = (TextView) itemView.findViewById(R.id.alba_pakinglist);
            texto = (TextView) itemView.findViewById(R.id.texto);
            cargados = (TextView) itemView.findViewById(R.id.cargados);
        }

        public void bindAlbaran(AlbaranDetalles c){
            if(c.getId() == -1){
                articulo.setText(R.string.articulo);
                alba_pakinglist.setText(R.string.packingList);
                texto.setText(R.string.por_cargar);
                cargados.setText(R.string.escaneados);
            }else{
                articulo.setText(c.getArticulo());
                alba_pakinglist.setText(""+c.getAlba_pakinglist());
                texto.setText("Cantidad: "+c.getTexto());
                cargados.setText("Cargados: "+c.getCargados());
            }
        }
    }
}
