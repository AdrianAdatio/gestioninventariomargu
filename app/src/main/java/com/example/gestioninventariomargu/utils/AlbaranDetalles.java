package com.example.gestioninventariomargu.utils;

import android.util.Log;

public class AlbaranDetalles {
    private int id;
    private long alba_pakinglist;
    private String texto;
    private int total;
    private int cargados;
    private int picking;
    private int caja;
    private String articulo;
    private int escaneados_ahora;

    public AlbaranDetalles() {
        id=-1;
    }

    public AlbaranDetalles(int id, long alba_pakinglist, String texto, int total, int cargados, int picking, int caja, String articulo) {
        this.id = id;
        this.alba_pakinglist = alba_pakinglist;
        this.texto = texto;
        if(picking == 2){
            this.texto += " ("+ (total / caja) + " cajas)";
        }
        this.total = total;
        this.cargados = cargados;
        this.picking = picking;
        this.caja = caja;
        this.escaneados_ahora = 0;
        this.articulo = articulo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getAlba_pakinglist() {
        return alba_pakinglist;
    }

    public void setAlba_pakinglist(long alba_pakinglist) {
        this.alba_pakinglist = alba_pakinglist;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getCargados() {
        return cargados;
    }

    public void setCargados(int cargados) {
        this.cargados = cargados;
    }

    public int getEscaneados_ahora() {
        return escaneados_ahora;
    }

    public void setEscaneados_ahora(int escaneados_ahora) {
        this.escaneados_ahora = escaneados_ahora;
    }

    public int getPicking() {
        return picking;
    }

    public void setPicking(int picking) {
        this.picking = picking;
    }

    public String getArticulo() {
        return articulo;
    }

    public void setArticulo(String articulo) {
        this.articulo = articulo;
    }

    public int getCaja() {
        return caja;
    }

    public void setCaja(int caja) {
        this.caja = caja;
    }

    public boolean scanOne(){
        if (total > cargados) {
            if(getPicking() == 2){
                this.cargados += caja;
                this.escaneados_ahora += caja;
            } else {
                this.cargados ++;
                this.escaneados_ahora ++;
            }
            return true;
        }
        return false;
    }

    public int getPor_Cargar(){
        return getTotal()-getCargados();
    }

    public boolean getFinalizado(){
        return getTotal() == getCargados();
    }

    @Override
    public String toString() {
        return "AlbaranDetalles{" +
                "id=" + id +
                ", alba_pakinglist=" + alba_pakinglist +
                ", texto='" + texto + '\'' +
                ", total=" + total +
                ", cargados=" + cargados +
                ", picking=" + picking +
                ", caja=" + caja +
                ", escaneados_ahora=" + escaneados_ahora +
                ", articulo='" + articulo + '\'' +
                "}\n";
    }
}
