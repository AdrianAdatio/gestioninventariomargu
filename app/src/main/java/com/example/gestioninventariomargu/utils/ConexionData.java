package com.example.gestioninventariomargu.utils;

public class ConexionData {
    private String ip;
    private String instance;
    private String table;
    private String user;
    private String pwd;
    private String pda;

    public ConexionData(String ip, String instance, String table, String user, String pwd, String pda) {
        this.ip = ip;
        this.instance = instance;
        this.table = table;
        this.user = user;
        this.pwd = pwd;
        this.pda = pda;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getInstance() {
        return instance;
    }

    public void setInstance(String instance) {
        this.instance = instance;
    }

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getPda() {
        return pda;
    }

    public void setPda(String pda) {
        this.pda = pda;
    }

    @Override
    public String toString() {
        return "ConexionData{" +
                "ip='" + ip + '\'' +
                ", instance='" + instance + '\'' +
                ", table='" + table + '\'' +
                ", user='" + user + '\'' +
                ", pwd='" + pwd + '\'' +
                ", pda='" + pda + '\'' +
                '}';
    }
}
