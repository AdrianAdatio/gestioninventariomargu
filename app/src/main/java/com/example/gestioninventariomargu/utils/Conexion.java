package com.example.gestioninventariomargu.utils;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.StrictMode;
import android.util.Log;
import android.widget.Toast;

import com.example.gestioninventariomargu.SQLite.SQLite;
import com.example.gestioninventariomargu.SQLite.SQLite_Calls;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Conexion {
    private static Connection con;

    public static Connection createConnection(ConexionData cd) {
        con = null;
        //Metodo para crear una conexion a SMSS con los valores recibidos
        try{
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            Class.forName("net.sourceforge.jtds.jdbc.Driver").newInstance();
            con = DriverManager.getConnection("jdbc:jtds:sqlserver://"+cd.getIp()+"/"+cd.getTable()+";instance="+cd.getInstance()+";user="+cd.getUser()+";password="+cd.getPwd()+";");
        } catch (Exception e) {
            Log.e("syso", "Error in conexionDB: "+e.getMessage());
        }
        return con;
    }

    public static Connection getConnection(Context context){
        //Si ya hay una conexion generada la devuelvo si no la creo con los Valores de SQLite
        if(testConexion(context)){
            return con;
        }else{
            return createFromDB(context);
        }
    }

    public static Connection createFromDB(Context context){
        //recupero los datos de Conexion de SQLite
        SQLiteDatabase db = SQLite.getDB(context);
        Cursor c = SQLite_Calls.getConexionData(db);
        if(c.getCount() != 0) {
            c.moveToNext(); //Cojo los datos de la ultima conexion que se guardo (hemos recuperado con "ORDER BY rowid DESC")
            ConexionData cp = new ConexionData(c.getString(0),c.getString(1),c.getString(2),c.getString(3),c.getString(4),c.getString(5));
            return createConnection(cp); //y los uso para crear una nueva conexion
        }
        return null; //Si se ha borrado la cache devuelvo null
    }

    private static boolean testConexion(Context context){
        if(con != null){
            try {
                Statement st = con.createStatement();
                ResultSet rs = st.executeQuery("SELECT TOP 5 id_cliente FROM TBL_CLIENTES;");
                while(rs.next()){
                    return true; //Si se pudo conectar a BD es que la conexion esta bien
                }
            } catch (SQLException e) {
                Log.e("syso", "testing Conexion: " + e.getMessage());
                Toast.makeText(context, "No se ha podido conectar a la red", Toast.LENGTH_SHORT).show();
            }
        }
        return false; //Si no se puede conectar o no había una conexion creada
    }
}
