package com.example.gestioninventariomargu.utils;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

public class ObjFromSSMS implements Parcelable {
    private int idObj;
    private String nombreObj;

    public ObjFromSSMS(int idObj, String nombreObj) {
        this.idObj = idObj;
        this.nombreObj = nombreObj;
    }

    public int getIdObj() {
        return idObj;
    }

    public void setIdObj(int idObj) {
        this.idObj = idObj;
    }

    public String getNombreObj() {
        return nombreObj;
    }

    public void setNombreObj(String nombreObj) {
        this.nombreObj = nombreObj;
    }

    @Override
    public String toString() {
        return "ObjFromSSMS{" +
                "idObj=" + idObj +
                ", nombreObj='" + nombreObj + '\'' +
                '}';
    }

    //COSAS DEL PARCELABLE, son necesarias para poder añadir el objeto en el intent

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(@NonNull Parcel parcel, int i) {
        parcel.writeInt(getIdObj());
        parcel.writeString(getNombreObj());
    }

    public static final Parcelable.Creator<ObjFromSSMS> CREATOR = new Parcelable.Creator<ObjFromSSMS>() {

        @Override
        public ObjFromSSMS createFromParcel(Parcel parcel) {
            return new ObjFromSSMS(parcel);
        }

        @Override
        public ObjFromSSMS[] newArray(int i) {
            return new ObjFromSSMS[i];
        }
    };

    public ObjFromSSMS(Parcel parcel) {
        this.idObj = parcel.readInt();
        this.nombreObj = parcel.readString();
    }
}
