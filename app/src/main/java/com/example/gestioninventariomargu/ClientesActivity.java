package com.example.gestioninventariomargu;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.gestioninventariomargu.utils.AdaptadorObjetos;
import com.example.gestioninventariomargu.utils.ObjFromSSMS;
import com.example.gestioninventariomargu.utils.Conexion;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class ClientesActivity extends AppCompatActivity {

    Toolbar toolbar;
    ImageButton logo;
    ArrayList<ObjFromSSMS> ClientesList;
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clientes);

        //con esto añado el toolbar para poder poner el logo y el boton de "atras"
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);

        //pequeño listener para volver a home si pulsamos el logo
        logo = findViewById(R.id.logo);
        logo.setOnClickListener(view -> {
            goBack();
        });

        recuperarClientes(); //Recupero listado de clientes de BD
        AdaptadorObjetos adaptadorClientes = new AdaptadorObjetos(ClientesList); //Y genero un adaptador con es lista

        //Controles del RecicleView para mostrar el listado
        recyclerView = findViewById(R.id.clientesRecView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(adaptadorClientes);

        //Cuando hago click en un cliente
        adaptadorClientes.setOnClickListener(view -> {
            goToEntrada(ClientesList.get(recyclerView.getChildAdapterPosition(view)));
        });
    }

    public void recuperarClientes(){
        ClientesList = new ArrayList<>();
        Connection con = Conexion.getConnection(this);
        try {
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery("SELECT id_cliente,nombre FROM TBL_CLIENTES WHERE baja = 0 ORDER BY nombre ASC");
            while (rs.next()){
                ClientesList.add(new ObjFromSSMS(rs.getInt("id_cliente"),rs.getString("nombre")));
            }
        } catch (SQLException e) {
            Log.e("syso", "select from TBL_CLIENTES: "+e.getMessage());
            Toast.makeText(getApplicationContext(),e.getMessage(),Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) { //Indico que voy a usar un menu en el Toolbar
        getMenuInflater().inflate(R.menu.back, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) { //Le doy funcionalidad a los items del menu
        int id = item.getItemId();

        if(id == R.id.back){
            goBack();
        }

        return true;
    }

    //Controles para cargar otras activities
    private void goBack(){
        this.onBackPressed();
    }

    private void goToEntrada(ObjFromSSMS c) {
        Intent intent = new Intent(getApplicationContext(), EntradaActivity.class);
        intent.putExtra("cliente", c); //Incluyo el cliente pulsado en el intent
        startActivity(intent);
    }
}