package com.example.gestioninventariomargu;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.gestioninventariomargu.utils.AdaptadorObjetos;
import com.example.gestioninventariomargu.utils.Conexion;
import com.example.gestioninventariomargu.utils.ObjFromSSMS;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class AlbaranesActivity extends AppCompatActivity {

    Toolbar toolbar;
    ImageButton logo;
    ArrayList<ObjFromSSMS> AlbaranesList;
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_albaranes);

        //con esto añado el toolbar para poder poner el logo y el boton de "atras"
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);

        //pequeño listener para volver a home si pulsamos el logo
        logo = findViewById(R.id.logo);
        logo.setOnClickListener(view -> {
            goBack();
        });

        recuperarAlbaranes(); //Recupero listado de albaranes de BD
        AdaptadorObjetos adaptadorAlbaranes = new AdaptadorObjetos(AlbaranesList); //Y genero un adaptador con esa lista

        //Controles del RecicleView para mostrar el listado
        recyclerView = findViewById(R.id.albaranesRecView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(adaptadorAlbaranes);

        //Cuando hago click en un albaran
        adaptadorAlbaranes.setOnClickListener(view -> {
            goToSalida(AlbaranesList.get(recyclerView.getChildAdapterPosition(view)));
        });
    }

    public void recuperarAlbaranes() {
        AlbaranesList = new ArrayList<>();
        Connection con = Conexion.getConnection(this);
        try {
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery("SELECT id,"+
                    "CASE WHEN a.tipo IS NOT NULL THEN CONCAT(c.nombre ,'\n', a.cont_matri ,'\n', a.n_albaran) ELSE CONCAT('\n','\n',a.n_albaran) END AS n_albaran "+
                    "FROM TBL_ALBARAN a " +
                    "LEFT JOIN TBL_CLIENTES c " +
                    "ON c.codigo = a.tipo "+
                    "WHERE salida = 0 ORDER BY fecha DESC");
            while (rs.next()) {
                AlbaranesList.add(new ObjFromSSMS(rs.getInt("id"), rs.getString("n_albaran")));
            }
        } catch (SQLException e) {
            Log.e("syso", "select from TBL_ALBARAN: " + e.getMessage());
            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) { //Indico que voy a usar un menu en el Toolbar
        getMenuInflater().inflate(R.menu.back, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) { //Le doy funcionalidad a los items del menu
        int id = item.getItemId();

        if (id == R.id.back) {
            goBack();
        }

        return true;
    }

    //Controles para cargar otras activities
    ActivityResultLauncher<Intent> activityResult = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {
                   recreate();
                }
            }
    );

    private void goBack() {
        this.onBackPressed();
    }

    private void goToSalida(ObjFromSSMS a) {
        Intent intent = new Intent(getApplicationContext(), SalidaActivity.class);
        intent.putExtra("albaran", a); //Incluyo el albaran pulsado en el intent
        activityResult.launch(intent);
    }
}