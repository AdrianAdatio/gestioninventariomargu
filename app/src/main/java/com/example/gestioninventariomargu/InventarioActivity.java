package com.example.gestioninventariomargu;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.gestioninventariomargu.SQLite.SQLite;
import com.example.gestioninventariomargu.SQLite.SQLite_Calls;
import com.example.gestioninventariomargu.utils.Conexion;
import com.example.gestioninventariomargu.utils.ObjFromSSMS;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class InventarioActivity extends AppCompatActivity {

    Toolbar toolbar;
    ImageButton logo;
    TextView syso, history;
    EditText scanedCode;
    Button guardarCambios;
    boolean first62 = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inventario);

        //con esto añado el toolbar para poder poner el logo y el boton de "atras"
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);

        //pequeño listener para volver a home si pulsamos el logo
        logo = findViewById(R.id.logo);
        logo.setOnClickListener(view -> {
            goBack();
        });

        //variables
        syso = findViewById(R.id.syso);
        scanedCode = findViewById(R.id.scanedCode);
        history = findViewById(R.id.history);
        guardarCambios = findViewById(R.id.guardarCambios);

        scanedCode.setShowSoftInputOnFocus(false);
        scanedCode.setInputType(0);

        //guardo los cambios en base de datos
        guardarCambios.setOnClickListener(view -> {
            if(saveCodes()){
                history.setText(""); //Borro el historial porque ya se han guardado
                goBack();
            }else{ //Si da error al guardarlos muestro mensaje de error
                Toast.makeText(getApplicationContext(),R.string.con_error,Toast.LENGTH_LONG).show();
            }
        });
    }

    //Recogemos el evento que se ejecuta cada vez que se va a escribir algo
    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        //Log.i("syso", "Codigo escaneado: "+event.getKeyCode()); //debuging
        if(event.getKeyCode()==62 && first62){
            first62 = false; //tengo que esperar al 2º espacio para no cortar caracteres
        }else if(event.getKeyCode()==62 && !first62){ //Buscamos el keycode que hemos indicado como fin del escaner
            first62 = true; //lo dejo como estaba para el siguiente
            String code = scanedCode.getText().toString();
            code = code.replaceAll("[\\D?!]",""); //Limpiamos el string
            MediaPlayer mediaPlayer = MediaPlayer.create(this, R.raw.beepbeep); //Preparamos el sonido de error

            //Si es EAN 128 lo pasamos a EAN 13
            if(code.length() == 28){
                code = code.substring(3, 16);
            } else if (code.length()==27) {
                code = code.substring(2, 15);
            } else if (code.length()==14) {
                code = code.substring(1);
            }

            if(code.length() >= 12 && code.length() <= 15){ //Comprobamos que se haya escaneado EAN 13
                String historyText = code+"\n"+history.getText().toString();
                history.setText(historyText); //Guardamos el nuevo codigo en la lista sin borrar los anteriores
                mediaPlayer = MediaPlayer.create(this, R.raw.beep); //cambio el sonido cuando es correcto
            }

            scanedCode.setText(""); //limpiamos el Edit Text se haya guardado o no
            if(code.length() > 0) mediaPlayer.start(); //Reproduzco el sonido si se escaneo algo
        }
        return super.dispatchKeyEvent(event); //devolvemos el evento para que siga el proceso normal
    }

    @Override
    public void onBackPressed() { //Capturo el evento de "back press" para guardar los codigos escaneados antes
        if(history.getText().toString() != ""){ //Compruebo si queda algun codigo por guardar
            alert().show(); //Si lo hay muestro el alert
        } else {
            super.onBackPressed();
        }
    }

    private Dialog alert(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.dialog_message); //mensaje
        builder.setPositiveButton(R.string.dialog_save, (dialog, id) -> { //boton ok
            if(saveCodes()){
                history.setText(""); //Borro el Historial porque ya se han guardado
                goBack(); //Y ejecuto el metodo para salir
            }else{ //Si da error al guardarlos muestro mensaje de error
                Toast.makeText(getApplicationContext(),R.string.con_error,Toast.LENGTH_LONG).show();
            }
        });
        builder.setNegativeButton(R.string.dialog_cancel, (dialog, id) -> { //boton cancel
            history.setText(""); //Borro el historial porque no quiero guardarlos
            goBack(); //Y ejecuto el metodo para salir
        });
        return builder.create(); //Lo creo y lo devuelvo para mostrarlo
    }

    private boolean saveCodes(){ //devuelve TRUE si permito ir "atras" y FALSE si aun hay cosas por guardar
        if(history.getText().toString() != "") {
            Connection con = Conexion.getConnection(this); //Recupero la conexion
            if (con != null) {
                try {
                    //Defino algunas variables para ahorrar tiempo de procesamiento
                    String pda_name = SQLite_Calls.getPdaName(SQLite.getDB(this));
                    String date = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(new Date());
                    String time = new SimpleDateFormat("HH:mm", Locale.getDefault()).format(new Date());

                    //Inserto cada codigo escaneado
                    PreparedStatement pst = con.prepareStatement("insert into TBL_INVENTARIO (codigo, fecha_entrada, hora_entrada, terminal_entrada, salida ,pakinglist, borrar) values (?,?,?,?,?,?,?)");
                    for (String code : history.getText().toString().split("\n")) {
                        int i = 0;
                        pst.setString(++i, code);
                        pst.setString(++i, date);
                        pst.setString(++i, time);
                        pst.setString(++i, pda_name);
                        pst.setInt(++i, 0);
                        pst.setString(++i, code);
                        pst.setInt(++i, 0);
                        pst.executeUpdate();
                    }
                    return true; //Si nada fallo procedo con el "BackPressed"
                } catch (SQLException e) {
                    Log.e("syso", "insert into TBL_INVENTARIO: " + e.getMessage());
                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                }
                //si llegue aqui, y la conexion no era null, significa que hubo un fallo de conexion y hay que volver a crearla
                Conexion.createFromDB(this);
            }
            return false; //Si no había conexion o algo fallo al intentar guardarlos
        }
        return true; //Si no se habia escaneado nada devuelvo true
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) { //Indico que voy a usar un menu en el Toolbar
        getMenuInflater().inflate(R.menu.back, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) { //Le doy funcionalidad a los items del menu
        int id = item.getItemId();

        if(id == R.id.back){
            goBack();
        }

        return true;
    }

    //Controles para cargar otras activities
    private void goBack(){
        this.onBackPressed();
    }

    private void goToHome(){
        Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
        startActivity(intent);
    }
}