package com.example.gestioninventariomargu.SQLite;

public class SQLite_Keys { //Constantes para la BD interna
    public static final String DB_NAME = "SQLite";
    public static final String TABLE_CON = "ConexionTable";
    public static final String COL_HOST = "hostCol";
    public static final String COL_INSTANCE = "instanceCol";
    public static final String COL_DATABASE = "databaseCol";
    public static final String COL_NAME = "userCol";
    public static final String COL_PWD = "pwdCol";
    public static final String COL_PDA_NAME = "pdaNameCol";
}
