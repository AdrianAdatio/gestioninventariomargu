package com.example.gestioninventariomargu.SQLite;

import static com.example.gestioninventariomargu.SQLite.SQLite_Keys.*;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class SQLite extends SQLiteOpenHelper { //Base de datos para guardar valores en memoria interna
    private static SQLite SQLiteSingleton;
    private static final int DATABASE_VERSION  = 3;

    //Patron Singleton para que solo exista una DB y esta solo tenga 1 punto de acceso
    public static SQLiteDatabase getDB(@Nullable Context context){
        if (SQLiteSingleton ==null){
            SQLiteSingleton = new SQLite(context);
        }
        return SQLiteSingleton.getWritableDatabase();
    }

    private SQLite(@Nullable Context context) {
        super(context, DB_NAME, null, DATABASE_VERSION );
    }

    //Crea la Base de datos con las columnas indicadas
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String sqlCreate = "CREATE TABLE " +
                TABLE_CON + " (" +
                COL_HOST + " TEXT, " +
                COL_INSTANCE + " TEXT, " +
                COL_DATABASE + " TEXT, " +
                COL_NAME + " TEXT, " +
                COL_PWD + " TEXT, " +
                COL_PDA_NAME + " TEXT)";
        sqLiteDatabase.execSQL(sqlCreate);
    }

    //Si cambia el numero de version se elimina la antigua y se crea una nueva
    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_CON);
        onCreate(sqLiteDatabase);
    }
}
