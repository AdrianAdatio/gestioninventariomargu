package com.example.gestioninventariomargu.SQLite;

import static com.example.gestioninventariomargu.SQLite.SQLite_Keys.*;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.gestioninventariomargu.utils.ConexionData;

public class SQLite_Calls { //Todas las llamadas a base de datos

    public static void saveConexionData(SQLiteDatabase db, ConexionData cd){
        //Genero un ContentValue que luego inserto en BD para guardar los valores de conexion y poder recuperarlos luego
        ContentValues cv = new  ContentValues();
        cv.put(COL_HOST, cd.getIp());
        cv.put(COL_INSTANCE, cd.getInstance());
        cv.put(COL_DATABASE, cd.getTable());
        cv.put(COL_NAME, cd.getUser());
        cv.put(COL_PWD, cd.getPwd());
        cv.put(COL_PDA_NAME, cd.getPda());
        db.insert(TABLE_CON, null, cv);
    }

    public static Cursor getConexionData(SQLiteDatabase db){
        //Recupero los valores de conexion que se han guardado en la Base de Datos
        String[] campos = new String[]{COL_HOST, COL_INSTANCE, COL_DATABASE, COL_NAME, COL_PWD, COL_PDA_NAME};
        String[] args = new String[]{"%"+"%"};
        String where = COL_NAME;

        Cursor c = db.query(
                TABLE_CON,              //Nombre de la tabla
                campos,                 //Lista de columnas a devolver
                where+" LIKE ?",        //1º Parte del Where (Donde comparamos y como)
                args,                   //2º parte del Where (Con que lo comparamos)
                null,                   //GROUP BY
                null,                   //HAVING del GROUP BY
                "rowid DESC"            //ORDER BY, "rowid" es un campo que genera SQLite por defecto con el id de cada objeto
        );

        return c;
    }

    public static String getPdaName(SQLiteDatabase db){
        //Recupero los valores de conexion que se han guardado en la Base de Datos
        String[] campos = new String[]{COL_PDA_NAME};
        String[] args = new String[]{"%"+"%"};
        String where = COL_PDA_NAME;

        Cursor c = db.query(
                TABLE_CON,              //Nombre de la tabla
                campos,                 //Lista de columnas a devolver
                where+" LIKE ?",        //1º Parte del Where (Donde comparamos y como)
                args,                   //2º parte del Where (Con que lo comparamos)
                null,                   //GROUP BY
                null,                   //HAVING del GROUP BY
                "rowid DESC"            //ORDER BY, "rowid" es un campo que genera SQLite por defecto con el id de cada objeto
        );

        if(c.getCount() != 0) {
            c.moveToNext(); //Cojo los datos de la ultima conexion que se guardo (hemos recuperado con "ORDER BY rowid DESC")
            return c.getString(0); //Y devuelvo el nombre de la PDA
        }

        return "Default";
    }
}
