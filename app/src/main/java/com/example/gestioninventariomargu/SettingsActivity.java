package com.example.gestioninventariomargu;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.gestioninventariomargu.utils.Conexion;
import com.example.gestioninventariomargu.SQLite.SQLite;
import com.example.gestioninventariomargu.SQLite.SQLite_Calls;
import com.example.gestioninventariomargu.utils.ConexionData;

import java.sql.Connection;

public class SettingsActivity extends AppCompatActivity {

    Toolbar toolbar;
    Button save;
    EditText ip,instance,table,user,pwd,pda;
    ImageButton logo;
    TextView syso, version;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        //con esto añado el toolbar para poder poner el logo y el boton de "atras"
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);

        //le doy valor a las variables
        ip = findViewById(R.id.ip);
        instance = findViewById(R.id.instance);
        table = findViewById(R.id.table);
        user = findViewById(R.id.user);
        pwd = findViewById(R.id.pwd);
        pda = findViewById(R.id.pda);
        syso = findViewById(R.id.syso);
        save = findViewById(R.id.save);
        version = findViewById(R.id.version);

        checkSavedData();

        save.setOnClickListener(view -> {
            //Creo la conexion con los valores indicados y si funciona guardo los valores en Base de Datos
            ConexionData cd = new ConexionData(ip.getText().toString(),instance.getText().toString(),table.getText().toString(),user.getText().toString(),pwd.getText().toString(),pda.getText().toString());
            Connection con = Conexion.createConnection(cd);
            if(con != null){
                //Recupero la base de datos y guardo los valores de conexion
                SQLiteDatabase db = SQLite.getDB(this);
                SQLite_Calls.saveConexionData(db,cd);

                syso.setText(R.string.con_ok); //Feedback
            }else{
                syso.setText(R.string.con_null); //Feedback
            }
        });

        //pequeño listener para volver a home si pulsamos el logo
        logo = findViewById(R.id.logo);
        logo.setOnClickListener(view -> {
            goBack();
        });

        version.setText(this.getResources().getString(R.string.copyright) + " / Version: " +BuildConfig.VERSION_NAME);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) { //Indico que voy a usar un menu en el Toolbar
        getMenuInflater().inflate(R.menu.back, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) { //Le doy funcionalidad a los items del menu
        int id = item.getItemId();

        if(id == R.id.back){
            goBack();
        }

        return true;
    }

    private void checkSavedData(){
        SQLiteDatabase db = SQLite.getDB(this);
        Cursor c = SQLite_Calls.getConexionData(db);
        if(c.getCount() != 0) {
            c.moveToNext(); //Cojo los datos de la ultima conexion que se guardo (hemos recuperado con "ORDER BY rowid DESC")
            int i = -1;
            ip.setText(c.getString(++i));
            instance.setText(c.getString(++i));
            table.setText(c.getString(++i));
            user.setText(c.getString(++i));
            pwd.setText(c.getString(++i));
            pda.setText(c.getString(++i));
        }
    }

    //Controles para cargar otras activities
    private void goBack(){
        this.onBackPressed();
    }

    private void goToHome(){
        Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
        startActivity(intent);
    }
}