package com.example.gestioninventariomargu;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.gestioninventariomargu.utils.Conexion;

import java.sql.Connection;

public class HomeActivity extends AppCompatActivity {
    TextView syso;
    Toolbar toolbar;

    Button entrada, salida, inventario, cerrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        //con esto añado el toolbar para poder poner el logo y el boton de "Settings"
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);

        //le doy valor a las variables
        syso = findViewById(R.id.syso);
        entrada = findViewById(R.id.entrada);
        salida = findViewById(R.id.salida);
        inventario = findViewById(R.id.inventario);
        cerrar = findViewById(R.id.cerrar);

        entrada.setOnClickListener(view -> {
            if(testConexion()){
                goToClientes();
            }
        });

        salida.setOnClickListener(view -> {
            if(testConexion()){
                goToAlbaranes();
            }
        });

        inventario.setOnClickListener(view -> {
            /*if(testConexion()){
                goToInventario();
            }*/
            Toast.makeText(getApplicationContext(), "Desabilitado por Administrador", Toast.LENGTH_LONG).show();
        });

        cerrar.setOnClickListener(view -> {
            if(testConexion()){
                closeApp();
            }
        });
    }

    private boolean testConexion(){ //Se intenta conectar y da Feedback
        Connection con = Conexion.getConnection(this);
        if(con != null){
            return true;
        }else{
            Toast.makeText(this,R.string.con_null,Toast.LENGTH_LONG).show();
            return false;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) { //Indico que voy a usar un menu en el Toolbar
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) { //Le doy funcionalidad a los items del menu
        int id = item.getItemId();

        if(id == R.id.ajustes){
            goToSettings();
        }

        return true;
    }

    //Controles para cargar otras activities
    private void goToSettings(){
        Intent intent = new Intent(getApplicationContext(), SettingsActivity.class);
        startActivity(intent);
    }
    private void goToClientes(){
        Intent intent = new Intent(getApplicationContext(), ClientesActivity.class);
        startActivity(intent);
    }
    private void goToEntrada(){
        Intent intent = new Intent(getApplicationContext(), EntradaActivity.class);
        startActivity(intent);
    }
    private void goToAlbaranes(){
        Intent intent = new Intent(getApplicationContext(), AlbaranesActivity.class);
        startActivity(intent);
    }
    private void goToSalida(){
        Intent intent = new Intent(getApplicationContext(), SalidaActivity.class);
        startActivity(intent);
    }
    private void goToInventario(){
        Intent intent = new Intent(getApplicationContext(), InventarioActivity.class);
        startActivity(intent);
    }
    private void closeApp(){
        finish();
    }
}